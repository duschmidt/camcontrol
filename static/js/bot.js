function delta(arg1,arg2)
	{
		arg2 = (typeof arg2 === "undefined" || arg2 == "") ? false : arg2; // default arg2 to false

		var Xmlhttp;
		var URL;
		var ARGS;
		
		xmlhttp = new XMLHttpRequest();
		//  example: back-end-page.php?arg1=something&arg2=false
		URL = "../delta?pan="+arg1+"&tilt="+arg2;
		xmlhttp.open("GET",URL,true);
		xmlhttp.send();
	}
function goto(arg1,arg2)
	{
		arg2 = (typeof arg2 === "undefined" || arg2 == "") ? false : arg2; // default arg2 to false

		var Xmlhttp;
		var URL;
		var ARGS;
		
		xmlhttp = new XMLHttpRequest();
		URL = "../goto?pan="+arg1+"&tilt="+arg2;
		xmlhttp.open("GET",URL,true);
		xmlhttp.send();
	}
	function sweep(arg1,arg2, arg3)
	{
		arg2 = (typeof arg2 === "undefined" || arg2 == "") ? false : arg2; // default arg2 to false

		var xmlhttp;
		var URL;
		var ARGS;
		
		xmlhttp = new XMLHttpRequest();
		URL = "../sweep?strt="+arg1+"&end="+arg2+"&pause="+arg3;
		
		xmlhttp.open("GET",URL,true);
		xmlhttp.send();
	}
	function gotoCoords()
	{
	  var pan = document.getElementById('txtPan').value;
	  var tilt = document.getElementById('txtTilt').value;
	  goto(pan,tilt);

	}

function dpad(direction)
	{
		document.getElementById("img_dpad").src = "images/dpad-" + direction + ".png";
		setTimeout('document.getElementById("img_dpad").src = "images/dpad.png";',200);
		document.getElementById("logArea").innerHTML = direction + "<br>" + document.getElementById("logArea").innerHTML;
		if (direction =='up')
			delta(0,5);
		else if(direction =='down')
			delta(0,-5);
		else if(direction =='left')
			delta(-5,0);
		else if(direction =='right')
			delta(5,0);
	}

function ab(aORb)
	{
		document.getElementById("img_ab").src = "images/ab-" + aORb + ".png";
		setTimeout('document.getElementById("img_ab").src = "images/ab.png";',200);
		document.getElementById("logArea").innerHTML = aORb + "<br>" + document.getElementById("logArea").innerHTML;
	}

function touchVStart(event)
	{
		document.getElementById("logArea").innerHTML = "touchVStart";
		
	}

	function touchVEnd(event)
	{
		document.getElementById("logArea").innerHTML = "touchVEnd";
		
	}

function touchVMove(event)
{
document.getElementById("logArea").innerHTML = "touchVMove";
		
}
if ('standalone' in navigator && !navigator.standalone && (/iphone|ipod|ipad/gi).test(navigator.platform) && (/Safari/i).test(navigator.appVersion)) {
	var addToHomeConfig = {
		animationIn:'drop',		// Animation In
		animationOut:'drop',		// Animation Out
		lifespan:600000,				// The popup lives basically forever
		touchIcon:true,
		message:'Install this web app on your %device by tapping %icon below and selecting Add to Home Screen.'
	};

	document.write('<link rel="stylesheet" href="css/add2home.css">');
	document.write('<script type="application\/javascript" src="js/add2home.js"><\/script>');
}


function doOnOrientationChange()
	{
		switch(window.orientation) 
			{  
				case -90:
				case 90:
					// Landscape
					document.getElementById("portrait_alert").style.display = "none";
					break; 
				default:
					// Portrait
					document.getElementById("portrait_alert").style.display = "block";
					break; 
			}
	}

window.onorientationchange = function()
	{
		doOnOrientationChange();
	};

$(document).ready(function()
{
	doOnOrientationChange();

});

