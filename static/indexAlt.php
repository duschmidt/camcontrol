<html>
<head>
<title>Rover UI Demo</title>

<meta name="viewport" content = "width = device-width, initial-scale = .5, user-scalable = no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" sizes="114x114" href="images/touch-icon-iphone4.png" />

<link rel="stylesheet" href="css/bot.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.min.js" /></script>
<script src="js/bot.js" type="text/javascript" charset="utf-8" /></script>

</head>
<body>

<div id="portrait_alert">This thing only<br>works in<br>landscape!<br><br><img src="images/rotate.png"></div>

<div id="ui_container">
<div id="videoArea">
<img src="http://x.arxian.com:8080/?action=stream" width="1136px" height="640px">
</div>

<div id="logArea">
</div>

<table border=0 cellpadding=0 cellspacing=0 height="100%" width="100%">
<tr>
	<td height="100%" width="100%" valign="bottom">
		<div id="div_dpad"><img src="images/dpad.png" class="dpad" id="img_dpad" usemap="#map_dpad" border="0"></div>
		<div id="div_ab"><img src="images/ab.png" class="ab" id="img_ab" usemap="#map_ab" border="0"></div>
	</td>
</tr>
</table>


<map name="map_dpad" id="map_dpad">
	<area shape="polygon" coords="0,0,122,122,244,0" href="#" onTouchStart="dpad('up');return false;" id="map_dpad_up">
	<area shape="polygon" coords="0,244,122,122,244,244" href="#" onTouchStart="touchVStart(event);return false;" id="map_dpad_down">
	<area shape="polygon" coords="0,0,122,122,0,244" href="#" onTouchStart="dpad('left');return false;" id="map_dpad_left">
	<area shape="polygon" coords="244,0,122,122,244,244" href="#" onTouchStart="dpad('right');return false;" id="map_dpad_right">
</map>

<map name="map_ab" id="map_ab">
	<area shape="circle" coords="63,152,63" href="#" onTouchStart="ab('a');return false;" id="map_ab_a">
	<area shape="circle" coords="182,63,63" href="#" onTouchStart="ab('b');return false;" id="map_ab_b">
</map>
</div>
</body>
</html>
