import serial
import subprocess
import time

ser = serial.Serial("/dev/ttyUSB0", 19200)
position = 0
ptemplate = "%ds"
ser.write(ptemplate%position)
call = "ffmpeg -f video4linux2 -i /dev/video0 -vframes 5 outfile.jpeg"

f = open("/home/pi/python/servo/main.html")
mainCode = f.read()
f.close()

from flask import Flask, send_file
from flask import request
app = Flask(__name__)



@app.route("/")
def main():
    global mainCode
    return mainCode
    #return "Hello World!"

@app.route("/goto/<int:pos>")
def goto(pos):
    global ser
    global position, ptemplate
    position = pos
    ser.write(ptemplate%position)
    #global call; subprocess.call(call.split(" "))

    #subprocess.call(["streamer","-c","/dev/video0","-b","16","-s","800x600","-o","outfile.jpeg"])
    #return send_file("outfile.jpeg")
    return str(position)

@app.route("/delta")
def delta():
    global ser, position, ptemplate
    shift = request.args.get("shift",0,type=int)
    print shift
    position += shift
    position = position % 180
    ser.write(ptemplate%position)
    return str(position)

@app.route("/sweep")
def sweep():
    global ser, position, ptemplate
    strt = request.args.get("strt",0,type=int) %180
    end = request.args.get("end",0,type=int) % 180
    pause = request.args.get("pause",0.0,type=float)

    at = strt
    while at != end:
        ser.write(ptemplate%at)
        at += 1
        time.sleep(pause)
    return str(end)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)
