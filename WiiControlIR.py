import time
import serial
import cwiid

ser =serial.Serial("/dev/ttyUSB0", 19200)


class GraphicApp():
	iPos = [128,128]
	panVel = 0
	tiltVel = 0
	deadZone = [122,134]
		
	def __init__(self):
		global ser

		ser.write("90p90t0r0h")

		self.mode = "VEL"
		print("Pair wiimote")
		self.wm = cwiid.Wiimote()
		self.wm.rpt_mode = cwiid.RPT_NUNCHUK

	def move(self):
		pt = None
		try:
			pt = self.wm.state['nunchuk']['stick']
		except:
			return
		infn = lambda a, b: a > b[0] and a < b[1]
		x = pt[0] if not infn(pt[0],self.deadZone) else self.iPos[0]
		y = pt[1] if not infn(pt[1],self.deadZone) else self.iPos[1]

		if self.mode == "VEL":
			global ser
			xAt = self.iPos[0] - x
			newVel = (int(xAt) / 10) * -10 * 2
			if self.panVel != newVel:
				self.panVel = newVel
				ser.write("%dr"%self.panVel)
				print "%dr"%self.panVel
			yAt = self.iPos[1] - y
			newVel = (int(yAt) / 10) * 10 * 2
			if self.tiltVel != newVel:
				self.tiltVel = newVel
				ser.write("%dh"%self.tiltVel)
				print "%dh"%self.tiltVel

	def mainloop(self):
		global ser
		
		ser.write("90p90t")
		while True:
			time.sleep(0.1)
			self.move()
		




	
		
	

graphicApp = GraphicApp()
graphicApp.mainloop()

ser.close()
