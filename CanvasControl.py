from Tkinter import *
import serial

ser =serial.Serial("/dev/ttyUSB0", 19200)


class GraphicApp(Frame):
	iPos = None
	panVel = 0
	tiltVel = 0
	def createWidgets(self):
		self.Surface = Canvas(self, width=200, height = 200, bg="#FFFFFF")
		

		self.Surface.bind('<Button-1>',self.click)
		self.Surface.bind('<B1-Motion>',self.move)
		self.Surface.bind('<ButtonRelease-1>',self.release)
		self.Surface.pack()
		
	def __init__(self, master=None):
		global ser
		ser.write("90p90t0r0h")
		self.mode = "VEL"
		self.master = master
		Frame.__init__(self, master)
		self.pack()
		self.createWidgets()
	
	def click(self,event):
		if self.mode =="VEL":
			print "click %d %d"%(event.x,event.y)
			self.iPos = (event.x,event.y)


	def move(self,event):
		if self.mode == "VEL":
			global ser
			xAt = self.iPos[0] - event.x
			newVel = (int(xAt) / 10) * 10 * 2
			if self.panVel != newVel:
				self.panVel = newVel
				ser.write("%dr"%self.panVel)
				print "%dr"%self.panVel
			yAt = self.iPos[1] - event.y
			newVel = (int(yAt) / 10) * 10 * 2
			if self.tiltVel != newVel:
				self.tiltVel = newVel
				ser.write("%dh"%self.tiltVel)
				print "%dh"%self.tiltVel

	def release(self,event):
		if self.mode == "VEL":
			global ser
			ser.write("0r0h")
			#ser.write("90p90t")
	
		
	

root = Tk()
graphicApp = GraphicApp(master=root)
graphicApp.mainloop()

ser.close()