import time
import serial
import cwiid
import sys

#sys.stdout = open("log.txt","a")
class Writer:

	def __init__(self, debug = False):
		self.debug = debug
		if not self.debug:
			self.writer =serial.Serial("/dev/ttyUSB0", 19200)


	def write(self, outStr):
		if self.debug:
			print outStr
		else:
			self.writer.write(outStr)

	def reset(self):
		if not self.debug:
			self.writer.close()
			self.writer.open()

ser = Writer(debug=False)


class GraphicApp():
	inRange = [1024, 768]
	iPos = [i/2 for i in inRange]
	deadZone = [0,0]
	maxV = 180		
	def __init__(self):
		global ser
		ser.write("s")
		connected = False
		#while not connected:
		#	try:
		print("Pair wiimote")
		
		wiiPaired = False
		attempts = 0
		maxAttempts = 100
		while(not wiiPaired and attempts < maxAttempts):
			try:
				#self.blink(0.5,5)
				self.wm = cwiid.Wiimote()
				wiiPaired = True
				#self.blink(0.25,10)
			except Exception, e:
				attempts += 1
				print("No wiimote found, trying again")
		if wiiPaired:
			self.wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_IR
		else:
			print("unable to pair")
			exit(1)
		#except Exception, e:
		#print(e)
				

	def move(self):
		global ser
		pt = None
		try:
			btn = self.wm.state['buttons']
			if bool(btn & cwiid.BTN_HOME):
				exit()
			if bool(btn & cwiid.BTN_A):
				ser.reset
			try:
				pt = self.wm.state['ir_src'][0]['pos']
			except:
				pt = self.iPos

			pt = list(pt)
			#pt[1] = self.iPos[1]

		except Exception, e:
			ser.write("s")
			print("error reading wiimote\n%s"%str(e))
			return
		infn = lambda a, b: a > b[0] and a < b[1]
		

		x = pt[0] #if not infn(pt[0],self.deadZone) else self.iPos[0]
		y = pt[1] #if not infn(pt[1],self.deadZone) else self.iPos[1]
		
		inRange = self.inRange
		yy = y - inRange[1]/2.0
		xx = x - inRange[0]/2.0
		yp = 0#yy / float(inRange[1]/2)
		xp = xx / float(inRange[0]/2)
		#print "pt: %0.8f       %0.8f"%tuple(pt)
		#print "xp: %0.8f   yp: %0.8f"%(xp,yp)
		maxV = self.maxV
		def v(i):
			if i > 254:
				return 254
			elif i < -254:
				return -254
			else:
				return i
		cmd = "%da%db"%(int(v(maxV*(yp+xp))),int(v(maxV*(yp-xp))))
		ser.write(cmd)
		

	def mainloop(self):
		global ser
		
		ser.write("s")
		while True:
			time.sleep(0.1)
			self.move()
		



	def blink(self, pause, count):
		import RPi.GPIO as gpio
		gpio.setwarnings(False)
		gpio.setmode(gpio.BCM)
		gpio.setup(16, gpio.OUT)
		for i in range(count):
			gpio.output(16, gpio.LOW)
			time.sleep(pause)
			gpio.output(16, gpio.HIGH)
			time.sleep(pause)
	
		
	

graphicApp = GraphicApp()
graphicApp.mainloop()

ser.close()
