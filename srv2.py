import serial 
import subprocess 
import time
from threading import Lock

lock = Lock()
ser = serial.Serial("/dev/ttyUSB0", 19200)
position = {"pan":90,"tilt":90}
ptemplate = ["%dp","%ds"]
limits = {"minPan":0,"maxPan":180,"minTilt":15,"maxTilt":180}

def setIn(min,max,val):
    if min > max:
        raise Exception("min > max")
    if val > max: 
        val = max 
    if val < min: 
        val = min
    return val

def moveTo(pan,tilt):
    with lock:
        global ser,position,setIn,limits
        if pan != None:
            position['pan'] = setIn(limits['minPan'],limits['maxPan'],pan)
            ser.write("%dp"%position['pan'])
        if tilt != None:
            position['tilt'] = setIn(limits['minTilt'],limits['maxTilt'],tilt)
            ser.write("%dt"%position['tilt'])

def setVel(pan,tilt):
    with lock:
        global ser
        ser.write("%dr%dh"%(pan,tilt))


f = open("/home/pi/python/servo/main.html")
mainCode = f.read()
f.close()

from flask import Flask, send_file
from flask import request
app = Flask(__name__, static_folder="static/")

if not app.debug and 1==2:
    import logging
    logger = logging.FileHandler('/var/tmp/flaskCam.log')
    logger.setLevel(logging.DEBUG)
    app.logger.addHandler(logger)




@app.route("/")
def main():
    global mainCode
    return mainCode
    #return "Hello World!"

@app.route("/goto")
def goto():
    global moveTo
    pan = request.args.get("pan",None,type=int)
    tilt = request.args.get("tilt",None,type=int)
    moveTo(pan,tilt)

    #subprocess.call(["streamer","-c","/dev/video0","-b","16","-s","800x600","-o","outfile.jpeg"])
    #return send_file("outfile.jpeg")
    return str(position)

@app.route("/delta")
def delta():
    with lock:
        global ser, position, ptemplate
        shiftPan = request.args.get("pan",None,type=int)
        shiftTilt = request.args.get("tilt",None,type=int)
        if shiftPan != None:
            position["pan"] += shiftPan
        if shiftTilt != None:
            position["tilt"] += shiftTilt
    moveTo(position["pan"],position["tilt"])
    
    return str(position)

@app.route("/sweep")
def sweep():
    global ser, position, ptemplate
    strt = request.args.get("strt",0,type=int) %180
    end = request.args.get("end",0,type=int) % 180
    pause = request.args.get("pause",0.0,type=float)

    at = strt
    while at != end:
        moveTo(at,None)
        at += 1
        time.sleep(pause)
    return str(end)

@app.route("/vel")
def vel():
    panVel = request.args.get("panVel",0,type=int)
    tiltVel = request.args.get("tiltVel",0,type=int)
    setVel(panVel,tiltVel)
    return "%dr%dh"%(panVel,tiltVel)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
