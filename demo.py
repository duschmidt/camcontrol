import serial
import subprocess

ser = serial.Serial("/dev/ttyUSB0", 19200)
position = "0"
ptemplate = "%ss"

def goto(pos):
    global ser, position, ptemplate
    position = str(pos)
    ser.write(ptemplate%position)
