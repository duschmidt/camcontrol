
function doSomething(arg1,arg2)
	{
		arg2 = (typeof arg2 === "undefined" || arg2 == "") ? false : arg2; // default arg2 to false

		var Xmlhttp;
		var URL;
		var ARGS;
		
		Xmlhttp = new XMLHttpRequest();
		//  example: back-end-page.php?arg1=something&arg2=false
		URL = "/path/to/back-end-page.php";
		ARGS = "arg1=" + arg1;
		if (arg2)
			{
				// do stuff if arg2 was provided
				ARGS += "&arg2=something";
			}
		else
			{
				// do stuff if arg2 was NOT provided, explicitely false
				ARGS += "&arg2=somethingElse";
			}
		
		Xmlhttp.open("POST", URL, true);
		
		Xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		Xmlhttp.setRequestHeader("Content-length", ARGS.length);
		Xmlhttp.setRequestHeader("Connection", "close");
		
		Xmlhttp.onreadystatechange = function()
			{//Call a function when the state changes.
				if(Xmlhttp.readyState == 4 && Xmlhttp.status == 200)
					{
						//alert(Xmlhttp.responseText);
						if(Xmlhttp.responseText == 1)
							{
								//alert("it worked!");
							}
						else
							{
								alert("You broke something...");
							}
					}
			}
		Xmlhttp.send(ARGS);
	}
