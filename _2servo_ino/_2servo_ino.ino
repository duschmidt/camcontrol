// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo servoPan;  // create servo object to control a servo 
Servo servoTilt;   // a maximum of eight servo objects can be created 

const int PAN = 0;
const int TILT = 1;

const int PIN_PAN=6;
const int PIN_TLT=5;
 
int panPos = 0;    // variable to store the servo position 
int tiltPos = 0;

int panVel = 0;
int tiltVel = 0;

int panMin = 25;
int panMax = 180;

int tiltMin = 15;
int tiltMax = 180;

unsigned long lastMillisPan = 0;
unsigned long lastMillisTilt = 0;


unsigned long millisWaitPan=0;
unsigned long millisWaitTilt = 0;

boolean servosOn = false;

void updateVel(){
 unsigned long time;
  time = millis();
  if (time - lastMillisPan >= millisWaitPan)
  {
     int sign=0;
     if (panVel > 0) sign = 1;
     else if (panVel<0) sign =-1;
     setPos(panPos + sign,PAN);
     lastMillisPan = time; 
  }
  if (time - lastMillisTilt >= millisWaitTilt)
  {
     int sign=0;
     if (tiltVel > 0) sign = 1;
     else if (tiltVel<0) sign =-1;
     setPos(tiltPos + sign,TILT);
     lastMillisTilt = time; 
  }
  
}

void setPos(int pos, int axis)
{
  if (axis == PAN && pos != panPos)
  {
    if (pos <= panMax && pos >= panMin)
    {
      panPos = pos;
      servoPan.write(pos);
    }
  }
  else if (axis == TILT && pos != tiltPos)
  {
    if (pos <= tiltMax && pos >= tiltMin)
    {
      tiltPos = pos;
      servoTilt.write(pos);
    }
  }
}

void setVel(int vel, int axis)
{
  if (axis == PAN && vel != panVel)
   {
     panVel = vel;
     millisWaitPan = 1000 / abs(panVel);
   }
  else if (axis = TILT && vel != tiltVel)
  {
     tiltVel = vel ;
     millisWaitTilt = 1000/abs(tiltVel);
  }
}
 
void setup() 
{ 
  servoTilt.attach(PIN_TLT);  // attaches the servo on pin 9 to the servo object 
  servoPan.attach(PIN_PAN);
  servosOn = true;
  Serial.begin(19200);
  Serial.println("Ready");

} 
 
 
void loop() 
{ 
  static int v = 0;
  static int sign = 1;
  if ( Serial.available()) {
    char ch = Serial.read();

    switch(ch) {
      case '0'...'9':
        v = v * 10 + ch - '0';
        break;
      case '-':
        sign = -1;
        break;
      case 't':
        setPos(sign*v,TILT);
        v = 0;
        sign = 1;
        break;
      case 'p':
        setPos(sign*v,PAN);
        v = 0;
        sign = 1;
        break;
      case 'r':
        setVel(sign*v,PAN);
        v = 0;
        sign=1;
        break;
      case 'h':
        setVel(sign*v,TILT);
        v=0;
        sign=1;
        break;
      case 'a':
        if (!servosOn)
        {
          servoTilt.attach(PIN_TLT);  // attaches the servo on pin 9 to the servo object 
          servoPan.attach(PIN_PAN);
          servosOn = true;
        }
        break;
      case 'd':
        if(servosOn)
        {
          servoPan.detach();
          servoTilt.detach();
          servosOn = false;
        }
        break;
    }
    
  } 
  updateVel();
}
