import time
import serial
import cwiid
import sys

#sys.stdout = open("log.txt","a")


ser =serial.Serial("/dev/ttyUSB0", 19200)
#ser = open('dump.txt', 'w')

class TankControl():
	iPos1 = [128,128]
	deadZone1 = [122,134]
	maxV = 180	
	iPos2 = [128,128]
	panVel = 0
	tiltVel = 0
	deadZone2 = [122,134]	
	def __init__(self):
		global ser
		ser.write("s")
		connected = False
		#while not connected:
		#	try:
		print("Pair wiimote")
		
		wiiPaired = False
		attempts = 0
		maxAttempts = 100
		while(not wiiPaired and attempts < maxAttempts):
			try:
				#self.blink(0.5,5)
				print("pair wiimote")
				self.wm = cwiid.Wiimote()
				wiiPaired = True
				#self.blink(0.25,10)
			except Exception, e:
				attempts += 1
				print("No wiimote found, trying again")
		if wiiPaired:
			self.wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_NUNCHUK
		else:
			print("unable to pair")
			exit(1)
		#except Exception, e:
		#print(e)
				
	def move(self):
		btn = self.wm.state['nunchuk']['buttons']
		if bool(btn & 1):
			self.moveTracks()
		if bool(btn & 2):
			self.moveTurret()



	def moveTurret(self):
		pt = None
		try:
			pt = self.wm.state['nunchuk']['stick']
		except:
			return
		infn = lambda a, b: a > b[0] and a < b[1]
		x = pt[0] if not infn(pt[0],self.deadZone2) else self.iPos2[0]
		y = pt[1] if not infn(pt[1],self.deadZone2) else self.iPos2[1]

		global ser
		xAt = self.iPos2[0] - x
		newVel = (int(xAt) / 10) * -10 * 2
		if self.panVel != newVel:
			self.panVel = newVel
			ser.write("%dr"%self.panVel)
			print "%dr"%self.panVel
		yAt = self.iPos2[1] - y
		newVel = (int(yAt) / 10) * 10 * 2
		if self.tiltVel != newVel:
			self.tiltVel = newVel
			ser.write("%dh"%self.tiltVel)
			print "%dh"%self.tiltVel

	def moveTracks(self):
		global ser
		pt = None
		try:
			btn = self.wm.state['buttons']
			if bool(btn & cwiid.BTN_HOME):
				exit()
			if bool(btn & cwiid.BTN_A):
				ser.close()
				ser.open()
			pt = self.wm.state['nunchuk']['stick']
		except Exception, e:
			ser.write("s")
			print("error reading wiimote\n%s"%str(e))
			return
		infn = lambda a, b: a > b[0] and a < b[1]
		

		x = pt[0] if not infn(pt[0],self.deadZone1) else self.iPos1[0]
		y = pt[1] if not infn(pt[1],self.deadZone1) else self.iPos1[1]
		yy = y - 128
		xx = x - 128
		yp = yy / 64.0
		xp = xx / 64.0
		maxV = self.maxV
		def v(i):
			if i > 254:
				return 254
			elif i < -254:
				return -254
			else:
				return i
		cmd = "%dx%dy"%(int(v(maxV*(yp+xp))),int(v(maxV*(yp-xp))))
		ser.write(cmd)
		print(cmd)

	def mainloop(self):
		global ser
		
		ser.write("s")
		while True:
			time.sleep(0.1)
			self.move()
		



	def blink(self, pause, count):
		import RPi.GPIO as gpio
		gpio.setwarnings(False)
		gpio.setmode(gpio.BCM)
		gpio.setup(16, gpio.OUT)
		for i in range(count):
			gpio.output(16, gpio.LOW)
			time.sleep(pause)
			gpio.output(16, gpio.HIGH)
			time.sleep(pause)
	
		

tank = TankControl()
tank.mainloop()

ser.close()
